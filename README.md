# cobox-group

Space class for cobox. Wraps `KappaDrive` in a custom encryption scheme for content encryption, enabling blind replication.

```
npm install cobox-group
```

## API

`const Space = require('cobox-group')`

`var space = Space(storage, key, opts)`

Returns a space object

- `storage` - path for storing feed data.  Defaults to `random-access-file`
- `key` - the space key (which may be a compound key containing a symmetric encryption key)
- `opts` an options object, which may contain:
  - `opts.config` a config object from `cobox-config`

Blind Replicator logic is exported as a separate function, to prevent empty indexes being created and run.

`const { Blind } = require('cobox-group')`
`var space = Blind(storage, key, opts)`

### `space.ready(callback)`

callback is called when the space is ready to use.

### `space.masterKey`

A master key for key derivation, to generate context specific keypairs for signing hypercores.

### `space.keys`

An object with properties `address` and `encryptionKey` as detailed below.

### `space.address`

The address or public key for the space

### `space.encryptionKey`

Random 32 byte key used to derive encryption key.

### `space.drive`

Your `KappaDrive` instance

### `space.state`

An interface to query your space drive changes, built with `kappa-view-query`.

### `space.log`

An additional hypercore for storing space-specific messages.

### `space.logs`

An interface to query your space log, built with `kappa-view-query`.

### `space.swarm(opts)`

Swarm across either Hyperswarm, or Discovery Swarm, based on opts.

### `space.bytesUsed()`

Returns the cumulative size of all of the spaces feeds.  Cannot be run before space is ready. Note that this is only the size of the data in the feeds, in reality a little more disk space is used to store keys, etc.
