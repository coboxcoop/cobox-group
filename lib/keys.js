const mkdirp = require('mkdirp')
const fs = require('fs')
const path = require('path')
const assert = require('assert')

function saveEncryptionKey (storage, encryptionKey) {
  var keyPath = path.join(storage, 'encryption_key')
  if (!fs.existsSync(keyPath)) {
    mkdirp.sync(path.dirname(keyPath))
    fs.writeFileSync(keyPath, encryptionKey, {
      mode: fs.constants.S_IRUSR
    })
  }

  // zero the encryption key buffer
  assert(fs.existsSync(keyPath), 'failed to store encryption_key')
}

// think: make fn take a callback and zero buffer after completed?
function loadEncryptionKey (storage) {
  try { return fs.readFileSync(path.join(storage, 'encryption_key')) }
  catch (err) { return }
}

module.exports = {
  loadEncryptionKey,
  saveEncryptionKey
}
