const { describe } = require('tape-plus')
const crypto = require('cobox-crypto')
const collect = require('collect-stream')
const debug = require('debug')('cobox-group')
const path = require('path')
const fs = require('fs')
const mkdirp = require('mkdirp')
const randomWords = require('random-words')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const mount = require('kappa-drive-mount')
const { Header } = require('hypertrie/lib/messages')
const SpaceAbout = require('cobox-schemas').encodings.space.about

const Space = require('../')

const { replicate, tmp, cleanup } = require('./util')

describe('cobox-space: Space', (context) => {
  context('constructor() - generates new encryption_key', (assert, next) => {
    var storage = tmp()
    var address = crypto.address().toString('hex')
    var name = randomWords(1).pop()

    var spy = sinon.spy(crypto)
    var ProxySpace = proxyquire('../', { 'cobox-crypto': spy })
    var space = ProxySpace(storage, address, { name })

    assert.ok(space, 'space created')
    assert.same(space.address, Buffer.from(address, 'hex'), 'has address')
    assert.same(space.name, name, 'has name')
    assert.ok(spy.encryptionKey.calledOnce, 'calls crypto.encryptionKey()')
    assert.ok(spy.encoder.calledOnce, 'uses encryption_key in encoder')

    cleanup(storage, next)
  })

  context('constructor() - loads encryption_key from path', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var encryptionKey = crypto.encryptionKey()
    var keyPath = path.join(storage, address.toString('hex'), 'encryption_key')

    mkdirp.sync(path.dirname(keyPath))
    fs.writeFileSync(keyPath, encryptionKey, { mode: fs.constants.S_IRUSR })

    var loadStub = sinon.stub().returns(encryptionKey)
    var saveStub = sinon.stub().returns(true)

    var ProxySpace = proxyquire('../', { 'cobox-keys': {
        loadKey: loadStub,
        saveKey: saveStub
      }
    })

    var space = ProxySpace(storage, address)

    assert.ok(loadStub.calledWith(space.path), 'key is loaded')
    assert.ok(saveStub.calledWith(space.path, 'encryption_key', encryptionKey), 'key is saved')
    cleanup(storage, next)
  })

  context('constructor() - saves encryption_key given as argument', (assert, next) => {
    var storage = tmp()
    var address = crypto.address().toString('hex')
    var encryptionKey = crypto.encryptionKey().toString('hex')
    var space = Space(storage, address, { encryptionKey })
    var key = fs.readFileSync(path.join(space.path, 'encryption_key'))

    assert.ok(space, 'space loaded')
    assert.ok(space.address, address, 'same address')
    assert.same(key.toString('hex'), encryptionKey.toString('hex'), 'encryption_key stored correctly')

    cleanup(storage, next)
  })

  // TODO: implement nanoresource properly
  // context('ready()', (assert, next) => {
  //   var storage = tmp()
  //   var address = crypto.address().toString('hex')
  //   var space = Space(storage, address)

  //   var spy = sinon.spy()
  //   space.ready(spy)
  //   space.ready(spy)

  //   setTimeout(() => {
  //     assert.ok(spy.calledOnce, 'ready only called once')
  //     cleanup(storage, next)
  //   }, 500)
  // })

  context('deriveKeyPair()', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var parentKey = crypto.masterKey()
    var deriveKeyPair = (id, ctxt) => crypto.keyPair(parentKey, id, ctxt)
    var space = Space(path.join(storage, 'spaces'), address, { deriveKeyPair, name: randomWords(1).pop() })

    space.ready(() => {
      assert.same(space.log._feed.key, deriveKeyPair(0, space.address).publicKey, 'Log key derived correctly')
      assert.same(space.log._feed.secretKey, deriveKeyPair(0, space.address).secretKey, 'Log secret key derived correctly')

      assert.same(space.drive.metadata.key, deriveKeyPair(1, space.address).publicKey, 'Metadata key derived correctly')
      assert.same(space.drive.metadata.secretKey, deriveKeyPair(1, space.address).secretKey, 'Metadata secret key derived correctly')

      assert.same(space.drive.content.key, deriveKeyPair(2, space.address).publicKey, 'Content key derived correctly')
      assert.same(space.drive.content.secretKey, deriveKeyPair(2, space.address).secretKey, 'Content secret key derived correctly')

      cleanup(storage, next)
    })
  })

  context('replicate() - basic', (assert, next) => {
    var storage1 = tmp(),
      storage2 = tmp(),
      address = crypto.address(),
      name1 = randomWords(1).pop(),
      name2 = randomWords(1).pop(),
      encryptionKey = crypto.encryptionKey()

    var base1 = Space(storage1, address, { encryptionKey, name: name1 })
    var base2 = Space(storage2, address, { encryptionKey, name: name2 })


    var dog = 'dog'
    var cat = 'cat'

    base1.ready(() => {
      base2.ready(() => {
        base1.feeds.writer('local', (err, feed1) => {
          assert.error(err, 'no error')

          base2.feeds.writer('local', (err, feed2) => {
            assert.error(err, 'no error')

            feed1.append(dog, (err, seq) => {
              assert.error(err, 'no error')

              feed2.append(cat, (err, seq) => {
                assert.error(err, 'no error')

                replicate(base1, base2, (err) => {
                  assert.error(err, 'no error on replicate')

                  var dup2 = base2.feeds.feed(feed1.key)
                  assert.ok(dup2, 'gets feed')
                  assert.equal(feed1.key.toString('hex'), dup2.key.toString('hex'), 'feed keys match')
                  assert.equal(dup2.length, 1, 'contains one message')
                  assert.notOk(dup2.secretKey, 'duplicate has no secret key')

                  var dup1 = base1.feeds.feed(feed2.key)
                  assert.ok(dup1, 'gets feed')
                  assert.equal(dup1.length, 1, 'contains one message')

                  cleanup([storage1, storage2], next)
                })
              })
            })
          })
        })
      })
    })
  })

  context('replicate() - drive', (assert, next) => {
    var storage1 = tmp(),
      storage2 = tmp(),
      address = crypto.address(),
      encryptionKey = crypto.encryptionKey(),
      name1 = randomWords(1).pop(),
      name2 = randomWords(1).pop()

    var space1 = Space(storage1, address, { encryptionKey, name: name1 }),
      space2 = Space(storage2, address, { encryptionKey, name: name2 })

    write(space1, 'world', () => {
      sync(() => {
        write(space2, 'mundo', () => sync(check))
      })
    })

    function write (space, data, cb) {
      space.ready((err) => {
        assert.error(err, 'no error')
        space1.drive.writeFile('/hello.txt', data, (err) => {
          assert.error(err, 'no error')
          cb()
        })
      })
    }

    function sync (cb) {
      replicate(space1, space2, (err) => {
        assert.error(err, 'no error')
        space1.ready(() => space2.ready(cb))
      })
    }

    function check () {
      space1.drive.readFile('/hello.txt', (err, data) => {
        assert.same(data, Buffer.from('mundo'), 'gets latest value')
        write(space2, 'verden', () => sync(checkAgain))
      })
    }

    function checkAgain () {
      space1.drive.readFile('/hello.txt', (err, data) => {
        assert.same(data, Buffer.from('verden'), 'gets latest value')
        cleanup([storage1, storage2], next)
      })
    }
  })

  // TODO: rewrite - CALLBACK HELL
  context('replicate() - complex', (assert, next) => {
    var storage1 = tmp(),
      storage2 = tmp(),
      timestamp = Date.now(),
      address = crypto.address(),
      encryptionKey = crypto.encryptionKey(),
      identity1 = crypto.boxKeyPair(),
      identity2 = crypto.boxKeyPair()

    var space1 = Space(storage1, address, { encryptionKey }),
      space2 = Space(storage2, address, { encryptionKey })

    var count = 0

    var content = [
      { filename: 'hello.txt', content: 'world' },
      { filename: 'world.txt', content: 'hello' }
    ]

    var msgs = [{
      type: 'space/about',
      author: identity1.publicKey.toString('hex'),
      timestamp: Date.now(),
      content: { name: 'Blockades' }
    }, {
      type: 'space/about',
      author: identity2.publicKey.toString('hex'),
      timestamp: Date.now() + 1,
      content: { name: 'Magma' }
    }]

    writeTo(space1, (err, _) => {
      assert.error(err, 'no error')

      writeTo(space2, (err, _) => {
        assert.error(err, 'no error')

        replicate(space1, space2, (err) => {
          assert.error(err, 'no error on replication')

          space1.ready(() => {
            space2.ready(() => {
              space1.drive.readdir('/', (err, files1) => {
                assert.error(err, 'no error')
                space2.drive.readdir('/', (err, files2) => {
                  assert.error(err, 'no error')
                  assert.same(files1, files2, 'replicates drive')

                  getAllMessages(space1.state, (err, state) => {
                    assert.error(err, 'no error')
                    var values = state
                      .map((msg) => msg.value)
                      .sort((a, b) => a.timestamp > b.timestamp ? +1 : -1)
                      .map((value) => value.filename)

                    assert.same(values, content.map((f) => f.filename), 'replicated drive changes')

                    space1.log.ready(() => {
                      getAllMessages(space1.log, (err, log) => {
                        assert.error(err, 'no error')
                        var values = log.map((msg) => msg.value)
                          .sort((a, b) => a.timestamp > b.timestamp ? +1 : -1)

                        assert.same(values, msgs, 'replicated messages')

                        space2.log.ready(() => {
                          getAllMessages(space2.state, (err, state) => {
                            assert.error(err, 'no error')
                            var values = state
                              .map((msg) => msg.value)
                              .sort((a, b) => a.timestamp > b.timestamp ? +1 : -1)
                              .map((value) => value.filename)

                            assert.same(values, content.map((f) => f.filename), 'replicated drive changes')

                            getAllMessages(space2.log, (err, log) => {
                              assert.error(err, 'no error')
                              var values = log
                                .map((msg) => msg.value)
                                .sort((a, b) => a.timestamp > b.timestamp ? +1 : -1)

                              assert.same(values, msgs, 'replicated messages')

                              return next()
                              cleanup([storage1, storage2], next)
                            })
                          })
                        })
                      })
                    })
                  })
                })
              })
            })
          })
        })
      })
    })

    function getAllMessages (handler, callback) {
      var query = [{ $filter: { value: { timestamp: { $gt: 0 } } } }]
      collect(handler.read({ query }), callback)
    }

    function getAllMessages (handler, callback) {
      var query = [{ $filter: { value: { timestamp: { $gt: 0 } } } }]
      collect(handler.read({ query }), callback)
    }

    function writeTo (space, cb) {
      space.ready((err) => {
        if (err) return cb(err)
        var file = content[count]
        space.drive.writeFile(file.filename, file.content, (err) => {
          if (err) return cb(err)
          var payload = msgs[count]
          space.log.publish(SpaceAbout.encode(payload), (err, msg) => {
            ++count
            cb(err)
          })
        })
      })
    }
  })
})
