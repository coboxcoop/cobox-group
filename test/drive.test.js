const { describe } = require('tape-plus')
const crypto = require('cobox-crypto')
const collect = require('collect-stream')
const debug = require('debug')('cobox-group')
const randomWords = require('random-words')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const { Header } = require('hypertrie/lib/messages')
const { setupLevel } = require('cobox-replicator/lib/level')
const path = require('path')
const multifeed = require('multifeed')
const Kappa = require('kappa-core')
const { KappaDrive } = require('kappa-drive')
const memdb = require('level-mem')

const DriveHandler = require('../lib/handlers/drive')

const { tmp, cleanup } = require('./util')

describe('cobox-group: DriveHandler', (context) => {
  context('constructor()', (assert, next) => {
    var storage = tmp()

    var handler = DriveHandler({
      storage,
      db: memdb(),
      address: crypto.address().toString('hex'),
      feeds: multifeed(storage),
      core: new Kappa(),
      deriveKeyPair: crypto.keyPair.bind(null, crypto.masterKey())
    })

    assert.ok(handler instanceof KappaDrive, 'extends KappaDrive')
    cleanup(storage, next)
  })

  context('ls()', (assert, next) => {
    var storage = tmp()

    var handler = DriveHandler({
      storage,
      db: memdb(),
      address: crypto.address().toString('hex'),
      feeds: multifeed(storage),
      core: new Kappa(),
      deriveKeyPair: crypto.keyPair.bind(null, crypto.masterKey())
    })

    handler.ready(() => {
      var filename = 'hello.txt'

      handler.writeFile(filename, 'world', async (err) => {
        assert.error(err, 'no error')

        var dir = await handler.ls()
        assert.equal(Object.keys(dir)[0], filename, 'lists file')
        assert.true(dir[filename].mtime > 10000, 'file has a timestamp')
        cleanup(storage, next)
      })

    })
  })

  context('mount()', async (assert, next) => {
    var storage = tmp()
    var stub = sinon.stub().resolves(new Promise((resolve, reject) => resolve()))
    var ProxyHandler = proxyquire('../lib/handlers/drive', { 'kappa-drive-mount': stub })

    var handler = ProxyHandler({
      storage,
      db: memdb(),
      address: crypto.address().toString('hex'),
      feeds: multifeed(storage),
      core: new Kappa(),
      deriveKeyPair: crypto.keyPair.bind(null, crypto.masterKey())
    })

    await handler.ready()

    try {
      await handler.mount()
    } catch (err) {
      assert.ok(err, 'throws an error')
      assert.same(err.message, 'provide a mount location', 'location required')
    }

    var opts = { location: './mnt' }
    var location = await handler.mount(opts)
    assert.same(location, './mnt', 'returns location')
    sinon.assert.calledOnce(stub)

    sinon.restore()
    cleanup(storage, next)
  })

  context('unmount()', async (assert, next) => {
    var storage = tmp()

    var handler = DriveHandler({
      storage,
      db: memdb(),
      address: crypto.address().toString('hex'),
      feeds: multifeed(storage),
      core: new Kappa(),
      deriveKeyPair: crypto.keyPair.bind(null, crypto.masterKey())
    })

    await handler.ready()

    try {
      await handler.unmount()
    } catch (err) {
      assert.ok(err, 'throws an error')
      assert.same(err.message, 'not mounted', 'not yet mounted')
    }

    handler.location = './mnt'
    handler._unmount = async () => {}
    var location = await handler.unmount()
    assert.same(location, './mnt', 'returns location')

    cleanup(storage, next)
  })
})
