const fs = require('fs')
const path = require('path')
const crypto = require('cobox-crypto')
const { describe } = require('tape-plus')
const { loadEncryptionKey, saveEncryptionKey } = require('../lib/keys')
const { tmp, cleanup } = require('./util')

describe('keys', (context) => {
  context('loadEncryptionKey()', (assert, next) => {
    var storage = tmp()
    var key = crypto.randomBytes(32)

    var keyPath = path.join(storage, 'encryption_key')
    fs.writeFileSync(keyPath, key, {
      mode: fs.constants.S_IRUSR
    })

    var encryptionKey = loadEncryptionKey(path.dirname(keyPath))
    assert.same(key, encryptionKey, `loads encryption_key from ${keyPath}`)
    cleanup(storage, next)
  })

  context('saveEncryptionKey()', (assert, next) => {
    var storage = tmp()
    var key = crypto.randomBytes(32)

    saveEncryptionKey(storage, key)

    var keyPath = path.join(storage, 'encryption_key')
    var encryptionKey = fs.readFileSync(keyPath, { mode: fs.constants.S_IRUSR })

    assert.same(key, encryptionKey, `saves encryption_key to ${keyPath}`)
    cleanup(storage, next)
  })
})
