const { describe } = require('tape-plus')
const crypto = require('cobox-crypto')
const collect = require('collect-stream')
const debug = require('debug')('cobox-group')
const randomWords = require('random-words')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const { Header } = require('hypertrie/lib/messages')
const { KappaDrive } = require('kappa-drive')
const { setupLevel } = require('cobox-replicator/lib/level')
const path = require('path')
const multifeed = require('multifeed')
const Kappa = require('kappa-core')
const memdb = require('level-mem')
const { tryDecode } = require('kappa-drive')

const Handler = require('../lib/handlers/state')

const { tmp, cleanup } = require('./util')

describe('cobox-group: StateHandler', (context) => {
  context('constructor()', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var deriveKeyPair = crypto.keyPair.bind(null, crypto.masterKey())
    var core = new Kappa()
    var feeds = multifeed(storage)
    var drive = new KappaDrive(storage, address, {
      core,
      multifeed: feeds,
      deriveKeyPair
    })

    var handler = Handler({
      db: memdb(),
      drive,
      core,
      feeds
    })

    assert.ok(handler.core instanceof Kappa, 'sets this.core')
    assert.ok(handler.feeds instanceof multifeed, 'sets this.feeds')
    assert.same(handler.drive, drive, 'sets this.drive')
    assert.ok(handler.db, 'sets this.db')

    cleanup(storage, next)
  })

  context('ready()', async (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var deriveKeyPair = crypto.keyPair.bind(null, crypto.masterKey())
    var core = new Kappa()
    var feeds = multifeed(storage)
    var drive = new KappaDrive(storage, address, {
      core,
      multifeed: feeds,
      deriveKeyPair
    })

    var handler = Handler({
      db: memdb(),
      drive,
      core,
      feeds
    })

    await handler.ready()

    assert.same(handler._feed, handler.drive.metadata, 'is the drive metadata feed')
    assert.ok(handler.core.view.state, 'creates a view')
    assert.same(handler.read, handler.core.view.state.read, 'binds view.read() to read()')

    drive.writeFile('./woof.txt',  'woof', (err) => {
      assert.error(err, 'no error')

      handler._feed.get(0, (err, msg) => {
        assert.error(err, 'no error')
        var header = Header.decode(msg)

        assert.same(header.type, 'hypertrie', 'is a hypertrie')
        cleanup(storage, next)
      })
    })
  })

  context('read()', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var deriveKeyPair = crypto.keyPair.bind(null, crypto.masterKey())
    var core = new Kappa()
    var feeds = multifeed(storage)
    var drive = new KappaDrive(storage, address, {
      core,
      multifeed: feeds,
      deriveKeyPair
    })

    var handler = Handler({
      db: memdb(),
      drive,
      core,
      feeds
    })

    handler.ready(() => {
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')

        handler._feed.get(2, (err, msg) => {
          var check = [{ key: handler._feed.key.toString('hex'), seq: 2, value: tryDecode(msg) }]

          handler.ready(() => {
            collect(handler.read({ query: [{ $filter: { value: { timestamp: { $gt: 0 } } } }] }), (err, msgs) => {
              assert.error(err, 'no error')
              assert.same(msgs.length, 1, 'gets one message')
              assert.same(msgs, check, 'message matches')
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })
})
