const path = require('path')
const thunky = require('thunky')
const through = require('through2')
const debug = require('debug')('cobox-group')
const assert = require('assert')
const maybe = require('call-me-maybe')
const crypto = require('cobox-crypto')
const { Replicator } = require('cobox-replicator')
const constants = require('cobox-constants')
const { loadKey, saveKey } = require('cobox-keys')
const { keyIds } = constants
const { log: LOG_ID } = keyIds

const Kappa = require('kappa-core')
const Log = require('cobox-log')
const Drive = require('./lib/handlers/drive')
const State = require('./lib/handlers/state')

const { setupLevel } = require('cobox-replicator/lib/level')

const ENC_KEY = 'encryption_key'

class Space extends Replicator {
  /**
   * Create a cobox space
   * @constructor
   */
  constructor (storage, address, opts = {}) {
    super(storage, address, opts)

    var key = loadKey(this.path, ENC_KEY) || opts.encryptionKey
    var encryptionKey = crypto.encryptionKey(key)
    assert(crypto.isKey(encryptionKey), `invalid: ${ENC_KEY}`)
    saveKey(this.path, ENC_KEY, encryptionKey)

    this._deriveKeyPair = opts.deriveKeyPair || randomKeyPair
    this.core = new Kappa()

    this._initFeeds({
      valueEncoding: crypto.encoder(encryptionKey, Object.assign({
        valueEncoding: this._opts.valueEncoding || 'utf8'
      }, this._opts))
    })
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      super.ready((err) => {
        if (err) return reject(err)
        this.open((err) => {
          if (err) return reject(err)
          this.log.ready((err) => {
            if (err) return reject(err)

            this.drive.ready((err) => {
              if (err) return reject(err)

              this.state.ready((err) => {
                if (err) return reject(err)

                this.core.ready((err) => {
                  if (err) return reject(err)
                  return resolve()
                })
              })
            })
          })
        })
      })
    }))
  }

  close (callback) {
    const done = () => {
      super.close((err) => {
        if (err) return callback(err)
        this.core.close(callback)
      })
    }

    if (!this.drive.isMounted()) return done()
    this.drive.unmount()
      .catch(callback)
      .then(done)
  }

  createConnectionsStream () {
    var self = this
    return this.db.connections
      .createLiveStream()
      .pipe(through.obj(function (msg, _, next) {
        if (msg.sync) return next()
        this.push({
          resourceType: 'GROUP',
          peerId: msg.key,
          address: hex(self.address),
          data: msg.value
        })
        next()
      }))
  }

  createLogStream () {
    var self = this
    let query = [{ $filter: { value: { timestamp: { $gt: 0 } } } }]
    return this.log
      .read({ query, live: true, old: true })
      .pipe(through.obj(function (msg, _, next) {
        if (msg.sync) return next()
        this.push({
          resourceType: 'SPACE',
          feedId: msg.key,
          address: hex(self.address),
          data: msg.value
        })
        next()
      }))
  }

  // ---------------------------------------------------------------- //

  _open (callback) {
    super._open((err) => {
      if (err) return callback(err)

      this.log = Log({
        core: this.core,
        feeds: this.feeds,
        author: hex(this.identity.publicKey),
        keyPair: this._deriveKeyPair(LOG_ID, this.address),
        db: setupLevel(path.join(this.path, 'views', 'log'))
      })

      this.drive = Drive({
        storage: this.storage,
        address: this.address,
        feeds: this.feeds,
        core: this.core,
        db: setupLevel(path.join(this.path, 'views', 'drive')),
        deriveKeyPair: this._deriveKeyPair
      })

      this.state = State({
        drive: this.drive,
        core: this.core,
        feeds: this.feeds,
        db: setupLevel(path.join(this.path, 'views', 'state'))
      })

      return callback()
    })
  }
}

function randomKeyPair (id, context) {
  return crypto.keyPair(crypto.masterKey(), id, context)
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf
  return buf.toString('hex')
}

module.exports = (storage, address, opts) => new Space(storage, address, opts)
module.exports.Space = Space
